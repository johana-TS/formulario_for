const genero= Array.from( document.querySelectorAll('input[type="radio"]'));
const hobbie= Array.from(document.querySelectorAll('input[type="checkbox"]'));
const btn= document.querySelector("#button");

function mostrar_datos (e){ //probar sin parametro e

    for (const elemento of genero){
        if (elemento.checked) {
            console.log(elemento.name, elemento.value);
        }
    }

    for (const key in hobbie) {
       if (hobbie[key].checked) {
           console.log(hobbie[key].name, hobbie[key].value);
       }
    }
}


btn.addEventListener("click",mostrar_datos);